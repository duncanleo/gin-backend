package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func loginHandler(c *gin.Context) {
	if c.PostForm("username") == "john" && c.PostForm("password") == "cake" {
		c.Redirect(http.StatusMovedPermanently, "/html/welcome.html")
		return
	}
	c.Redirect(http.StatusMovedPermanently, "/html/nope.html")
}

func indexHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}

func main() {
	c := gin.Default()
	c.LoadHTMLGlob("html/*.html")
	c.GET("/", indexHandler)
	c.Static("/html", "html/")
	c.POST("/login", loginHandler)
	c.Run(":8080")
}
